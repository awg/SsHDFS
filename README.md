# sshdfs

R package for simple remote access to HDFS via ssh

+ setup password less ssh connection to your Hadoop cluster

+ either via ssh key or kerberos (eg use kinit before running the R script)

```R
library(sshdfs)
## return matching files info as data frame
dir.df <- hdfs.ls("project/data-2016/01", host="hadoop_box")

## prints resulting data frame

        perm  user      group      size                    path               mdate
1 drwxr-xr-x dirkd supergroup         0           .sparkStaging 2016-03-16 12:08:00
2 drwxr-xr-x dirkd supergroup         0                 airline 2014-11-18 09:12:00
3 -rw-r--r-- dirkd supergroup 707364897                   e.csv 2014-11-14 15:59:00
4 drwxr-xr-x dirkd supergroup         0                     eos 2014-11-14 14:23:00
5 -rw-r--r-- dirkd supergroup        93               input.txt 2014-11-17 16:27:00
6 -rw-r--r-- dirkd supergroup        93              input2.txt 2014-11-17 16:28:00
7 -rw-r--r-- dirkd supergroup  36414767 lemonbi201602oneday.csv 2016-03-14 16:40:00
8 drwxr-xr-x dirkd         zg         0                 linkage 2016-02-11 16:58:00


## read a file into a local data frame
data.df <- read.csv(hdfs.cat("project/data-2016/01/file1.csv", host="hadoop_box"))

## use data.df as usual...

## alternatively:

## copy a file to local working directory
hdfs.copyToLocal("project/data-2016/01/file1.csv")

```
